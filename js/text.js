    // Get a reference to the editor container element
    var editorElement = document.getElementById('editor');

    // Initialize the Quill editor
    var quill = new Quill(editorElement, {
      modules: {
        toolbar: [
          [{ 'font': [] }, { 'size': [] }],
          [ 'bold', 'italic', 'underline', 'strike' ],
          [{ 'color': [] }, { 'background': [] }],
          [{ 'script': 'super' }, { 'script': 'sub' }],
          [{ 'header': '1' }, { 'header': '2' }, 'blockquote', 'code-block' ],
          [{ 'list': 'ordered' }, { 'list': 'bullet'}, { 'indent': '-1' }, { 'indent': '+1' }],
          [ 'direction', { 'align': [] }],
          [ 'link', 'image', 'video', 'formula' ],
          [ 'clean' ]
      ]
      },
      placeholder: 'Compose an epic...',
      theme: 'snow'
    });

    // Add event listener to download button
    document.getElementById('download-pdf').addEventListener('click', function() {
      // Create a new div element to hold the editor content
      var contentDiv = document.createElement('div');
      contentDiv.innerHTML = quill.root.innerHTML;

      // Create a new jsPDF instance
      var doc = new jsPDF();

      // Convert the contentDiv to a PDF and add it to the document
      doc.fromHTML(contentDiv, 15, 15, {
        'width': 170
      });

      // Download the PDF
      doc.save('document.pdf');
    });