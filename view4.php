<?php
// This file is part of Moodle - https://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <https://www.gnu.org/licenses/>.

/**
 *
 * @package     mod_programlab
 * @copyright   2023 Luis German <lugecardenas2000@gmail.com> and Julian A. Cardona <jacardonas@udistrital.edu.co>
 * @license     https://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once('../../config.php');
require_once($CFG->dirroot. '/mod/programlab/lib.php');

$id = optional_param('id', 0, PARAM_INT); // Course_module ID.
$n = optional_param('n', 0, PARAM_INT);  // ... programlab instance ID - it should be named as the first character of the module.

if ($id) {
    $cm = get_coursemodule_from_id('programlab', $id, 0, false, MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $cm->course), '*', MUST_EXIST);
    $programlab = $DB->get_record('programlab', array('id' => $cm->instance), '*', MUST_EXIST);
} else if ($n) {
    $programlab = $DB->get_record('programlab', array('id' => $n), '*', MUST_EXIST);
    $course = $DB->get_record('course', array('id' => $programlab->course), '*', MUST_EXIST);
    $cm = get_coursemodule_from_instance('programlab', $programlab->id, $course->id, false, MUST_EXIST);
} else {
    error('You must specify a course_module ID or an instance ID');
}

require_login($course, true, $cm);


$event = \mod_programlab\event\course_module_viewed::create(array(
    'objectid' => $PAGE->cm->instance,
    'context' => $PAGE->context,
));
$event->add_record_snapshot('course', $PAGE->course);
$event->add_record_snapshot($PAGE->cm->modname, $programlab);
$event->trigger();

// Print the page header.

$PAGE->set_url('/mod/programlab/view.php', array('id' => $cm->id));
$PAGE->set_title(format_string($programlab->name));
$PAGE->set_heading(format_string($course->fullname));

/*
 * Other things you may want to set - remove if not needed.
 * $PAGE->set_cacheable(false);
 * $PAGE->set_focuscontrol('some-html-id');
 * $PAGE->add_body_class('newmodule-'.$somevar);
 */

// Output starts here.
echo $OUTPUT->header();

// Conditions to show the intro can change to look for own settings or whatever.
if ($programlab->intro) {
    echo $OUTPUT->box(format_module_intro('programlab', $programlab, $cm->id), 'generalbox mod_introbox', 'newmoduleintro');
}

// Adding Moodle/ to this redirections works for server
$templatecontext[info] = "/mod/programlab/view.php?id=$id#view.php";
$templatecontext[texto] = "/mod/programlab/view2.php?id=$id#view2.php";
$templatecontext[editor] = "/mod/programlab/view3.php?id=$id#view3.php";
$templatecontext[acercade] = "/mod/programlab/view4.php?id=$id#view4.php";

echo $OUTPUT->render_from_template('mod_programlab/acercade', $templatecontext );
// Finish the page.
